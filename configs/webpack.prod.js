/* eslint-disable */

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const autoPrefixer = require('autoprefixer');
const CopyPlugin = require('copy-webpack-plugin');

const gitInfo = new GitRevisionPlugin();

module.exports = {
  mode: 'production',
  entry: './src/index.tsx',
  output: {
    filename: 'build.js',
    path: path.resolve('build'),
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
  },
  // devtool: 'inline-module-source-map', // turn this on to debug prod build
  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
      {
        test:/\.(s*)css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader?url=false',
            options: {
              modules: {
                localIdentName: '[name]-[local]-[hash:base64:5]'
              }
            }
          },
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: function () {
                return [ autoPrefixer];
              }
            }
          },
          {
            loader: 'sass-loader',
          },
        ]
      },
      {
        test: /\.(ttf|png|jpg|jpeg|gif|pdf|ico)$/,
        use: [
          'file-loader'
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      commitHash: JSON.stringify(gitInfo.commithash()),
      template: 'src/app/index.ejs',
      favicon:'public/images/favicon.ico'
    }),
    new MiniCssExtractPlugin(),
    new CopyPlugin( [
        { from: 'public/fonts', to: 'public/fonts' },
        { from: 'public/pdfs', to: 'public/pdfs' },
      ],
    ),
  ],
};
