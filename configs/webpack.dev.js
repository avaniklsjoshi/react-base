/* eslint-disable */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const autoPrefixer = require('autoprefixer');
const gitInfo = new GitRevisionPlugin();

module.exports = {
  mode: 'development',
  entry: ['react-hot-loader/patch', './src/index.tsx'],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build'),
  },
  devtool: 'inline-module-source-map',
  devServer: {
    historyApiFallback: true,
    hot: true
 },
  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.scss'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
      {
        test:/\.(s*)css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader?url=false',
            options: {
              modules: {
                localIdentName: '[name]-[local]-[hash:base64:5]'
              }
            }
          },
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: function () {
                return [ autoPrefixer];
              }
            }
          },
          {
            loader: 'sass-loader',
          },
        ]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|pdf|ico)$/,
        use: [
          {
            loader: 'file-loader',
          }
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      commitHash: JSON.stringify(gitInfo.commithash()),
      template: 'src/app/index.ejs',
    }),
    new MiniCssExtractPlugin(),
  ],
};
