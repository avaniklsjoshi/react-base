
export const RESUME_URLS=[
    {path:'./public/pdfs/Avani Joshi Cover letter.pdf', name:'Avani Joshi Cover letter.pdf'},
    {path:'./public/pdfs/Resume.pdf', name:'Avani Joshi Resume.pdf'},
    {path:'./public/pdfs/Avani Joshi Frontend Developer.pdf', name:'Avani Joshi Frontend Developer.pdf'},
    {path:'./public/pdfs/Avani Joshi Projects Worked on.pdf', name:'Avani Joshi Projects Worked on.pdf'}
];
