export enum PAGE_NAMES {
  LANDING_PAGE = 'landing',
  FEATURES_PAGE = 'features',
  CONTACT_PAGE = 'contact',
}

export enum ROUTE_NAMES {
  LANDING_PAGE = '/',
  FEATURES_PAGE = '/features',
  CONTACT_PAGE = '/contact',
  ABOUT_PAGE='/about'
}

export enum PAGE_DISPLAY_NAMES {
  LANDING_PAGE = 'Home',
  FEATURES_PAGE = 'Features',
  CONTACT_PAGE = 'Contact',
  ABOUT_PAGE='About'
}

export enum Breakpoints {
  default = 0,
  tablet = 768,
  desktop = 1440,
  max = 1920,
  b560 = 560,
  b960 = 960,
  b1280 = 1280,
  b1600 = 1600,
  b1920 = 1920,
  b2560 = 2560
}

export enum LANGUAGES {
  ENGLISH = 'EN',
  HINDI = 'HIN',
  GERMAN = 'GER'
}