// This file is for default values
import { LANGUAGES } from './enums';

export const DEFAULT_LANGUAGE = LANGUAGES.ENGLISH;