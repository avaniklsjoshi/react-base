/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useState} from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Styles from './Styles.scss';

const useStyles = makeStyles(() =>
  createStyles({
    formControl: {
      minWidth: 80,
      height: 40,
    },
    selectBox:{
        fontSize: 14,
        lineHeight: 1,
        color: '#1c05a0'
    },
    menuItem:{
        fontSize: 14,
        lineHeight: 1,
        color: 'gray',
    }
  }),
);

export function Dropdown(props: any) {
    const {defaultVal, list, drdName, handleClick} = props;
    const [currentVal, setCurrentVal] = useState(defaultVal || '');
    const classes = useStyles();

    const handleChange = (event: any) => {
        setCurrentVal(event.target.value);
        handleClick(event.target.value);
    };

    return (
      <div className={Styles.dropdownContainer}>
        <FormControl className={classes.formControl}>
          <Select
            id={drdName}
            value={currentVal}
            onChange={handleChange}
            className={classes.selectBox}
          >
            {list && list.length && list.map((key: any)=>{
            return <MenuItem value={key} key={key} className={classes.menuItem}>{key}</MenuItem>
          })}
          </Select>
        </FormControl>
      </div>
    );
}

export default Dropdown;
