import React from 'react';
import Styles from './SocialNetwork.scss';
import {  Twitter } from '../../../shared/svgIcons/Twitter';
import {  LinkedIn } from '../../../shared/svgIcons/LinkedIn';
import {  Github } from '../../../shared/svgIcons/Github';
import {  Gitlab } from '../../../shared/svgIcons/Gitlab';

export default function SocialNetwork() {
  return (
    <div className={Styles.social}>
      <a href='https://twitter.com/avaniklsjoshi/' target='_blank' rel="noopener noreferrer" aria-label="Twitter Avani">
        <Twitter />
      </a>
      <a href='https://www.linkedin.com/in/avani-joshi-6365b887/' target='_blank' rel="noopener noreferrer" aria-label="LinkedIn Avani">
        <LinkedIn />
      </a>
      <a href='https://github.com/avaniklsjoshi' target='_blank' rel="noopener noreferrer" aria-label="github Avani">
        <Github />
      </a>
      <a href='https://gitlab.com/avaniklsjoshi' target='_blank' rel="noopener noreferrer" aria-label="gitlab Avani">
        <Gitlab />
      </a>
    </div>
);
}
