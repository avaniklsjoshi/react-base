import React from 'react';
import Styles from './PDFService.scss';
import { RESUME_URLS as urls } from '../../../shared/constants';

export default function PDFService() {

  const downloadAll=(evt: any)=>{
    evt.preventDefault();

    const link = document.createElement('a');
    link.style.display = 'none';

    document.body.appendChild(link);
    for (let i = 0; i < urls.length; i += 1) {
      link.setAttribute('href', urls[i].path);
      link.setAttribute('download',urls[i].name);
      link.click();
    }
    document.body.removeChild(link);
  }

  return (
    <div className={Styles.resumeContainer}>
      <div className={Styles.resumeTitle}> Download expensive documents here!</div>
      <div className={Styles.resumeButtons}>
        <a href='./public/pdfs/Resume.pdf' download>Resume</a>
        <a href='./public/pdfs/Avani Joshi Cover letter.pdf' download>Cover Letter</a>
        <a href='./public/pdfs/Avani Joshi Projects Worked on.pdf' download>Projects</a>
        <a href='./public/pdfs/Avani Joshi Frontend Developer.pdf' download>All in one Resume</a>
        <button onClick={downloadAll}>Download All</button>
      </div>
    </div>
);
}
