import React from 'react';
import { NavLink } from 'react-router-dom';
import { PAGE_DISPLAY_NAMES, ROUTE_NAMES } from '../../../shared/enums';
import Styles from './NavigationHeader.scss';

export default function NavigationHeader() {
  return (
    <ul className={Styles.navigator}>
      <li>
        <NavLink to={ROUTE_NAMES.LANDING_PAGE} exact activeClassName={Styles.activeNavItem}>
          {PAGE_DISPLAY_NAMES.LANDING_PAGE}
        </NavLink>
      </li>
      <li>
        <NavLink to={ROUTE_NAMES.FEATURES_PAGE} activeClassName={Styles.activeNavItem}>
          {PAGE_DISPLAY_NAMES.FEATURES_PAGE}
        </NavLink>
      </li>
      <li>
        <NavLink to={ROUTE_NAMES.ABOUT_PAGE} activeClassName={Styles.activeNavItem}>
          {PAGE_DISPLAY_NAMES.ABOUT_PAGE}
        </NavLink>
      </li>
      <li>
        <NavLink to={ROUTE_NAMES.CONTACT_PAGE} activeClassName={Styles.activeNavItem}>
          {PAGE_DISPLAY_NAMES.CONTACT_PAGE}
        </NavLink>
      </li>
    </ul>
);
}
