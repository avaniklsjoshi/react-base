import React from 'react';
import Styles from './Footer.scss';
import SocialNetwork from '../SocialNetworks/SocialNetwork';

export default function Footer() {
  return (
    <div className={Styles.footer}>
      <SocialNetwork />
      <hr />
      <span className={Styles.copyRight}>
        Copyright © 2020 Avani Joshi - All Rights Reserved.
      </span>
    </div>
);
}
