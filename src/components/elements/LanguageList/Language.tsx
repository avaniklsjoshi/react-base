import React from 'react';
import { useTranslation } from "react-i18next";
import Dropdown from '../../common/Dropdown';
import { LANGUAGES } from '../../../shared/enums';
import { DEFAULT_LANGUAGE } from '../../../shared/default';

export default function Language() {
  const { i18n } = useTranslation();

  const selectedLang = (newLanguage: string) => {
    i18n.changeLanguage(newLanguage);
  };

  return <Dropdown defaultVal={DEFAULT_LANGUAGE} list={Object.values(LANGUAGES)} drdName='selectLanguage' handleClick={selectedLang} />;
}
