import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";
import {  ROUTE_NAMES } from '../../../shared/enums';
import NavigationHeader from '../NavigationHeader/NavigationHeader';
import Styles from './Header.scss';
import Language from '../LanguageList/Language'

export default function Header() {
  const { t } = useTranslation();

  return (
    <div className={Styles.headerContainer}>
      <div className={Styles.owner}>
        <Link to={ROUTE_NAMES.LANDING_PAGE}>
          {t('homeTitle')}
        </Link>
      </div>
      <NavigationHeader />
      <Language />
    </div>
);
}
