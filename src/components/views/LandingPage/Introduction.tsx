import React from 'react';
import Icon from '../../../../public/images/work.jpg';
import Styles from './LandingPage.scss';

export default function Introduction() {
  return (
    <div className={Styles.landingPageContainer}>
      <img className={Styles.landingBackground} alt='Avani Joshi on work' src={Icon}  />
    </div>
);
}
