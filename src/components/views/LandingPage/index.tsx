import React from 'react';
import PDFServices from '../../elements/PDFServices/PDFService'
import Styles from './LandingPage.scss';
import Introduction from './Introduction';

export default function LandingPage() {
  return (
    <div className={Styles.landingPageContainer}>
      <Introduction />
      <PDFServices />
    </div>
);
}
