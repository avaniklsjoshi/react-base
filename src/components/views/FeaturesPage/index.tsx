import React from 'react';
import Counter from '../../elements/Counter/Counter';
import StarHeroes from '../../elements/StarHeroes/StarHeroes';
import StarShips from '../../elements/StarShips/StarShips';
import Planets from '../../elements/Planets/Planets';

export default function FeaturesPage() {
  return (
    <div>
      <Counter />
      <StarHeroes />
      <StarShips />
      <Planets />
    </div>
  );
}
