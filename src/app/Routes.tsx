import React from 'react';
import { Route, Switch } from 'react-router';
import { ROUTE_NAMES } from '../shared/enums';
import ContactPage from '../components/views/ContactPage/index';
import FeaturesPage from '../components/views/FeaturesPage/index';
import LandingPage from '../components/views/LandingPage/index';
import AboutPage from '../components/views/AboutPage/index';
import NotfoundPage from '../components/views/NotfoundPage/index';

export default function Routes() {
  return (
    <Switch>
      <Route exact path={ROUTE_NAMES.LANDING_PAGE} component={LandingPage} />
      <Route path={ROUTE_NAMES.FEATURES_PAGE} exact component={FeaturesPage} />
      <Route path={ROUTE_NAMES.ABOUT_PAGE} exact component={AboutPage} />
      <Route path={ROUTE_NAMES.CONTACT_PAGE} exact component={ContactPage} />
      <Route component={NotfoundPage} />
    </Switch>
  );
}
