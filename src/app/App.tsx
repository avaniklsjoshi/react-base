import { hot } from 'react-hot-loader/root';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { I18nextProvider } from "react-i18next";
import Styles from './App.scss';
import store from '../configurations/redux';
import Routes from './Routes';
import Header from '../components/elements/Header/Header';
import Footer from '../components/elements/Footer/Footer';
import i18n from "../shared/i18n";

const App: React.FC = () => {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <BrowserRouter basename='/'>
          <div className={Styles.App}>
            <Header />
            <div className={Styles.pages}>
              <Routes />
            </div>
            <Footer />
          </div>
        </BrowserRouter>
      </Provider>
    </I18nextProvider>
  );
};

export default hot(App);
