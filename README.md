# React-base

It will be base for react app, having common features

## `Things still to happen hold tight`

- environment variables, global variables
- Pipeline, Docker, different environments stage prod qa
- tracking
- SEO
- web security
- awesome project setup ideas

- api fail UI
- theming
- scroll loader at top
- 404 page
- click scroll to section
- service workers
- PWA
- Cookie
- caching
- lazy loading
- canvas, webGL
- svg images
- https
- express local
- WebAssembly, Svelte
- awesome feature ideas

- Webpack settings- chunking, compression, publish, Profiling, bundle analyser
- Optimisation, lighthouse chrome
- npm package
- monorepo

- respective backend- AWS, Dyanamo DB, lambda, s3

## `Topics covered`

- Type script
- webpack
- eslint/tslint
- husky
- lint-staged
- prettier
- hot reloading
- Routing
- saga
- react-redux
- storybook
- html generation
- scss
- user agent stylesheet remove
- fonts
- assets
- autoprefixer, post pre css
- font load
- i18
- Navigation bar, https://codepen.io/erikterwan/pen/EVzeRP

* multiple webpack file mgmt
* test cases, coverage
